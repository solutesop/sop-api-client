<?php
namespace SoluteSop\Api\Response;

class Order extends ResponseAbstract
{

	protected $_orderId = null;

	protected $_email = null;

	protected $_sequence = null;

	protected $_cartId = null;

	protected $_supplierId = null;

	protected $_createdAt = null;

	protected $_updatedAt = null;

	protected $_cartReference = null;

	protected $_perCartSequence = null;

	protected $_shippingAddress1 = null;

	protected $_shippingAddress2 = null;

	protected $_shippingAddress3 = null;

	protected $_shippingCity = null;

	protected $_shippingState = null;

	protected $_shippingPostalCode = null;

	protected $_shippingCountry = null;

	protected $_shippingPhoneNumber = null;

	protected $_items = array();

	/**
	 *
	 * @var bool
	 */
	protected $_canceled = false;

	protected $_hydratorClass = 'SoluteSop\Api\Response\Hydrator\Order';

	/**
	 * @return Order
	*/
	public function setOrderId($value)
	{
		$this->_orderId = (int) $value;
		return $this;
	}
	public function getOrderId()
	{
		return (int) $this->_orderId;
	}


	public function isCanceled()
	{
	    foreach ($this->getItems() as $item) {
	        /** @var $item OrderItem */
	        if (!$item->getCanceled()) {
	            return false;
	        }
	    }

	    return true;
	}

	/**
	 * @return Order
	 */
	public function setSequence($value)
	{
		$this->_sequence = (int) $value;
		return $this;
	}
	public function getSequence()
	{
		return (int) $this->_sequence;
	}


	/**
	 * @return Order
	 */
	public function setCartId($value)
	{
		$this->_cartId = (int) $value;
		return $this;
	}
	public function getCartId()
	{
		return (int) $this->_cartId;
	}


	/**
	 * @return Order
	 */
	public function setSupplierId($value)
	{
		$this->_supplierId = (int) $value;
		return $this;
	}
	public function getSupplierId()
	{
		return (int) $this->_supplierId;
	}


	/**
	 * @return Order
	 */
	public function setCreatedAt(\DateTime $value)
	{
		$this->_createdAt = $value;
		return $this;
	}
	public function getCreatedAt()
	{
		return $this->_createdAt;
	}


	/**
	 * @return Order
	 */
	public function setUpdatedAt(\DateTime $value)
	{
		$this->_updatedAt = $value;
		return $this;
	}
	public function getUpdatedAt()
	{
		return $this->_updatedAt;
	}


	/**
	 * @return Order
	 */
	public function setCartReference($value)
	{
		$this->_cartReference = (string) $value;
		return $this;
	}
	public function getCartReference()
	{
		return (string) $this->_cartReference;
	}


	/**
	 * @return Order
	 */
	public function setPerCartSequence($value)
	{
		$this->_perCartSequence = (int) $value;
		return $this;
	}
	public function getPerCartSequence()
	{
		return (int) $this->_perCartSequence;
	}


	/**
	 * @return Order
	 */
	public function setShippingAddress1($value)
	{
		$this->_shippingAddress1 = (string) $value;
		return $this;
	}
	public function getShippingAddress1()
	{
		return (string) $this->_shippingAddress1;
	}


	/**
	 * @return Order
	 */
	public function setShippingAddress2($value)
	{
		$this->_shippingAddress2 = (string) $value;
		return $this;
	}
	public function getShippingAddress2()
	{
		return (string) $this->_shippingAddress2;
	}


	/**
	 * @return Order
	 */
	public function setShippingAddress3($value)
	{
		$this->_shippingAddress3 = (string) $value;
		return $this;
	}
	public function getShippingAddress3()
	{
		return (string) $this->_shippingAddress3;
	}


	/**
	 * @return Order
	 */
	public function setShippingCity($value)
	{
		$this->_shippingCity = (string) $value;
		return $this;
	}
	public function getShippingCity()
	{
		return (string) $this->_shippingCity;
	}


	/**
	 * @return Order
	 */
	public function setShippingState($value)
	{
		$this->_shippingState = (string) $value;
		return $this;
	}
	public function getShippingState()
	{
		return (string) $this->_shippingState;
	}


	/**
	 * @return Order
	 */
	public function setShippingPostalCode($value)
	{
		$this->_shippingPostalCode = (string) $value;
		return $this;
	}
	public function getShippingPostalCode()
	{
		return (string) $this->_shippingPostalCode;
	}


	/**
	 * @return Order
	 */
	public function setShippingCountry($value)
	{
		$this->_shippingCountry = (string) $value;
		return $this;
	}
	public function getShippingCountry()
	{
		return (string) $this->_shippingCountry;
	}


	/**
	 * @return Order
	 */
	public function setShippingPhoneNumber($value)
	{
		$this->_shippingPhoneNumber = (string) $value;
		return $this;
	}
	public function getShippingPhoneNumber()
	{
		return (string) $this->_shippingPhoneNumber;
	}




	public function addItem(OrderItem $value)
	{
		$this->_items[$value->getOrderItemId()] = $value;
		return $this;
	}
	public function getItems()
	{
		return $this->_items;
	}

	/**
	 * @return Order
	 */
	public function setEmail($value)
	{
		if (filter_var($value, FILTER_VALIDATE_EMAIL) === false) {
			throw new \UnexpectedValueException('Value "' . $value . '" is no email');
		}
		$this->_email = (string) $value;
		return $this;
	}
	public function getEmail()
	{
		return (string) $this->_email;
	}


}

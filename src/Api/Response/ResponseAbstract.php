<?php
namespace SoluteSop\Api\Response;

abstract class ResponseAbstract implements ResponseInterface
{

	/**
	 *
	 * @var json
	 */
	protected $_rawData = null;

	/**
	 *
	 * @var HydratorInterface
	 */
	protected $_hydratorObject = null;

	/**
	 *
	 * @var string
	 */
	#protected $_hydratorClass = 'SoluteSop\Api\Response\Hydrator\Orders';
	protected $_hydratorClass = null;

	/**
	 *
	 * {@inheritDoc}
	 * @see \SoluteSop\Api\Response\ResponseInterface::setRawData()
	 */
	public function setRawData($value)
	{
		if (is_object($value)) {
			$value = json_encode($value);
		}
		$this->_rawData = $value;
		return $this;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \SoluteSop\Api\Response\ResponseInterface::getRawData()
	 */
	public function getRawData()
	{
		return $this->_rawData;
	}

	/**
	 *
	 * @param HydratorInterface $value
	 * @return \SoluteSop\Api\Response\ResponseAbstract
	 */
	public function setHydratorObject(HydratorInterface $value)
	{
		$this->_hydratorObject = $value;
		return $this;
	}

	/**
	 *
	 * @return \SoluteSop\Api\Response\Hydrator\HydratorInterface
	 */
	public function getHydratorObject()
	{
		if ($this->_hydratorObject === null) {
			$class = $this->getHydratorClass();
			$hydrator = new $class();
			$this->_hydratorObject = $hydrator;
		}
		return $this->_hydratorObject;
	}

	/**
	 *
	 * @param string $value
	 * @return \SoluteSop\Api\Response\ResponseAbstract
	 */
	public function setHydratorClass($value)
	{
		$this->_hydratorClass = $value;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getHydratorClass()
	{
		return $this->_hydratorClass;
	}

}

<?php
namespace SoluteSop\Api\Response\Hydrator;

use SoluteSop\Api\Response\Orders as OrdersResponse;

class Orders extends HydratorAbstract
{


	/**
	 * just for typehinting
	 * @param Orders $orders
	 */
	protected function _hydrate(OrdersResponse $response)
	{
		$rows = json_decode($response->getRawData());
		$rows = $rows->orders;
		foreach ($rows as $key => $row) {
			$order = $response->getNewEmptyOrder();
			$order->setRawData($row);
			$order->getHydratorObject()->hydrate($order);

			// @todo Order setter / getter schreiben
			#\Zend_Debug::dump($order);
			$response->add($order);
		}

		return $response;
	}


}
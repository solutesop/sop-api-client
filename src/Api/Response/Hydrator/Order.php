<?php
namespace SoluteSop\Api\Response\Hydrator;

use SoluteSop\Api\Response\Order as OrderResponse;
use SoluteSop\Api\Response\OrderItem;

class Order extends HydratorAbstract
{



	protected function _hydrate(OrderResponse $response)
	{
		$rawData = json_decode($response->getRawData());
		$response->setOrderId($rawData->id)
			->setCartId($rawData->cart_id)
			->setSequence($rawData->sequence)
			->setCreatedAt(new \DateTime($rawData->created_at))
			->setUpdatedAt(new \DateTime($rawData->updated_at))
			->setSupplierId($rawData->supplier_id)
			->setCartReference($rawData->cart_reference)
			->setPerCartSequence($rawData->per_cart_sequence)
			->setShippingAddress1($rawData->shipping_address_1)
			->setShippingAddress2($rawData->shipping_address_2)
			->setShippingAddress3($rawData->shipping_address_3)
			->setShippingCity($rawData->shipping_city)
			->setShippingState($rawData->shipping_state)
			->setShippingPostalCode($rawData->shipping_postal_code)
			->setShippingCountry($rawData->shipping_country)
			->setShippingPhoneNumber($rawData->shipping_phone_number)
			#->setShippingOrderlines($rawData->orderlines)
		;

		foreach ($rawData->orderlines as $row) {
			$orderItemResponse = new OrderItem();
			$orderItemResponse->setRawData($row);
			$orderItemResponse->getHydratorObject()->hydrate($orderItemResponse);
			$response->addItem($orderItemResponse);
		}

		return $this;
	}

}

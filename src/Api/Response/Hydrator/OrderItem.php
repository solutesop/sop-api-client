<?php
namespace SoluteSop\Api\Response\Hydrator;

use SoluteSop\Api\Response\OrderItem as OrderItemResponse;

class OrderItem extends HydratorAbstract
{


	protected function _hydrate(OrderItemResponse $response)
	{
		$rawData = json_decode($response->getRawData());
		$response->setOrderItemId($rawData->id)
    		->setLineSequence($rawData->line_sequence)
    		->setOrderId($rawData->order_id)
    		->setCartlineId($rawData->cartline_id)
    		->setQuantityOrdered($rawData->quantity_ordered)
    		->setQuantityConfirmed($rawData->quantity_confirmed)
    		->setQuantityShipped($rawData->quantity_shipped)
    		->setCanceled($rawData->canceled)
    		->setAmount($rawData->item_price)
			->setShippingAmount($rawData->item_delivery_costs)
			->setSupplierShare($rawData->supplier_share)
			->setShopShare($rawData->shop_share)
			->setOrderedAt(new \DateTime($rawData->ordered_at))
			->setConfirmedAt(new \DateTime($rawData->confirmed_at))
			->setShippedAt(new \DateTime($rawData->shipped_at))
			->setCreatedAt(new \DateTime($rawData->created_at))
			->setUpdatedAt(new \DateTime($rawData->updated_at))
			->setCanceledAt(new \DateTime($rawData->canceled_at))
			->setShippingCarrier($rawData->shipping_carrier)
			->setShippingCarrierTrackingId($rawData->shipping_carrier_tracking_id)
			->setLineReference($rawData->line_reference)
			->setOfferCode($rawData->offer_code)
			->setItemCode($rawData->item_code)
			->setItemTitle($rawData->item_title)
			->setMainCategory($rawData->main_category)
			->setReturnShipmentStatus($rawData->return_shipment_status)
		;
		return $this;
	}


}
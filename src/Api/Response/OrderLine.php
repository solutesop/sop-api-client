<?php
namespace SoluteSop\Api\Response;

class OrderLine extends ResponseAbstract
{
	protected $_error = null;

	protected $_hydratorClass = 'SoluteSop\Api\Response\Hydrator\OrderLine';


	/**
	 * @return OrderLine
	 */
	public function setError($value)
	{
		$this->_error = (string) $value;
		return $this;
	}
	public function getError()
	{
		return (string) $this->_error;
	}

}
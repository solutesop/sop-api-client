<?php
namespace SoluteSop\Api\Response;

class OrderItem extends ResponseAbstract
{

	/**
	 *
	 * @var string
	 */
	protected $_hydratorClass = 'SoluteSop\Api\Response\Hydrator\OrderItem';

	protected $_orderItemId = null;

	protected $_orderId = null;

	protected $_lineSequence = null;

	protected $_cartlineId = null;

	protected $_quantityOrdered = null;

	protected $_quantityConfirmed = null;

	protected $_quantityShipped = null;

	protected $_canceled = null;

	/**
	 *
	 * @var null
	 */
	protected $_returnShipmentStatus = null;


	protected $_amount = null;

	protected $_shippingAmount = null;

	protected $_supplierShare = null;

	protected $_shopShare = null;

	protected $_orderedAt = null;

	protected $_shippedAt = null;

	protected $_canceledAt = null;

	protected $_confirmedAt = null;

	protected $_createdAt = null;

	protected $_updatedAt = null;

	protected $_shippingCarrier = null;

	protected $_shippingCarrierTrackingId = null;

	protected $_lineReference = null;

	protected $_offerCode = null;

	protected $_itemCode = null;

	protected $_itemTitle = null;

	protected $_mainCategory = null;

	/**
	 * @return OrderItem
	 */
	public function setOrderItemId($value)
	{
		$this->_orderItemId = (int) $value;
		return $this;
	}
	public function getOrderItemId()
	{
		return (int) $this->_orderItemId;
	}


	/**
	 * @return OrderItem
	 */
	public function setOrderId($value)
	{
		$this->_orderId = (int) $value;
		return $this;
	}
	public function getOrderId()
	{
		return (int) $this->_orderId;
	}


	/**
	 * @return OrderItem
	 */
	public function setLineSequence($value)
	{
		$this->_lineSequence = (int) $value;
		return $this;
	}
	public function getLineSequence()
	{
		return (int) $this->_lineSequence;
	}


	/**
	 * @return OrderItem
	 */
	public function setCartlineId($value)
	{
		$this->_cartlineId = (int) $value;
		return $this;
	}
	public function getCartlineId()
	{
		return (int) $this->_cartlineId;
	}


	/**
	 * @return OrderItem
	 */
	public function setQuantityOrdered($value)
	{
		$this->_quantityOrdered = (int) $value;
		return $this;
	}
	public function getQuantityOrdered()
	{
		return (int) $this->_quantityOrdered;
	}


	/**
	 * @return OrderItem
	 */
	public function setQuantityConfirmed($value)
	{
		$this->_quantityConfirmed = (int) $value;
		return $this;
	}
	public function getQuantityConfirmed()
	{
		return (int) $this->_quantityConfirmed;
	}

	public function setCanceled($value)
	{
	    $this->_canceled = (int) $value;
	    return $this;
	}
	public function getCanceled()
	{
	    return (int) $this->_canceled;
	}

	/**
	 * @return OrderItem
	 */
	public function setQuantityShipped($value)
	{
		$this->_quantityShipped = (int) $value;
		return $this;
	}
	public function getQuantityShipped()
	{
		return (int) $this->_quantityShipped;
	}


	/**
	 * @return OrderItem
	 */
	public function setAmount($value)
	{
		$this->_amount = (double) $value;
		return $this;
	}
	public function getAmount()
	{
		return (double) $this->_amount;
	}


	/**
	 * @return OrderItem
	 */
	public function setShippingAmount($value)
	{
		$this->_shippingAmount = (double) $value;
		return $this;
	}
	public function getShippingAmount()
	{
		return (double) $this->_shippingAmount;
	}


	/**
	 * @return OrderItem
	 */
	public function setSupplierShare($value)
	{
		$this->_supplierShare = (double) $value;
		return $this;
	}
	public function getSupplierShare()
	{
		return (double) $this->_supplierShare;
	}


	/**
	 * @return OrderItem
	 */
	public function setShopShare($value)
	{
		$this->_shopShare = (double) $value;
		return $this;
	}
	public function getShopShare()
	{
		return (double) $this->_shopShare;
	}


	/**
	 * @return OrderItem
	 */
	public function setOrderedAt(\DateTime $value)
	{
		$this->_orderedAt = $value;
		return $this;
	}
	public function getOrderedAt()
	{
		return $this->_orderedAt;
	}


	/**
	 * @return OrderItem
	 */
	public function setShippedAt(\DateTime $value)
	{
		$this->_shippedAt = $value;
		return $this;
	}
	public function getShippedAt()
	{
		return $this->_shippedAt;
	}

	/**
	 * @return OrderItem
	 */
	public function setCanceledAt(\DateTime $value)
	{
	    $this->_canceledAt = $value;
	    return $this;
	}
	public function getCanceledAt()
	{
	    return $this->_canceledAt;
	}

	/**
	 * @return OrderItem
	 */
	public function setConfirmedAt(\DateTime $value)
	{
		$this->_confirmedAt = $value;
		return $this;
	}
	public function getConfirmedAt()
	{
		return $this->_confirmedAt;
	}


	/**
	 * @return OrderItem
	 */
	public function setCreatedAt(\DateTime $value)
	{
		$this->_createdAt = $value;
		return $this;
	}
	public function getCreatedAt()
	{
		return $this->_createdAt;
	}


	/**
	 * @return OrderItem
	 */
	public function setUpdatedAt(\DateTime $value)
	{
		$this->_updatedAt = $value;
		return $this;
	}
	public function getUpdatedAt()
	{
		return $this->_updatedAt;
	}


	/**
	 * @return OrderItem
	 */
	public function setShippingCarrier($value)
	{
		$this->_shippingCarrier = (string) $value;
		return $this;
	}
	public function getShippingCarrier()
	{
		return (string) $this->_shippingCarrier;
	}


	/**
	 * @return OrderItem
	 */
	public function setShippingCarrierTrackingId($value)
	{
		$this->_shippingCarrierTrackingId = (string) $value;
		return $this;
	}
	public function getShippingCarrierTrackingId()
	{
		return (string) $this->_shippingCarrierTrackingId;
	}


	/**
	 * @return OrderItem
	 */
	public function setLineReference($value)
	{
		$this->_lineReference = (string) $value;
		return $this;
	}
	public function getLineReference()
	{
		return (string) $this->_lineReference;
	}


	/**
	 * @return OrderItem
	 */
	public function setOfferCode($value)
	{
		$this->_offerCode = (string) $value;
		return $this;
	}
	public function getOfferCode()
	{
		return (string) $this->_offerCode;
	}


	/**
	 * @return OrderItem
	 */
	public function setItemCode($value)
	{
		$this->_itemCode = (string) $value;
		return $this;
	}
	public function getItemCode()
	{
		return (string) $this->_itemCode;
	}


	/**
	 * @return OrderItem
	 */
	public function setItemTitle($value)
	{
		$this->_itemTitle = (string) $value;
		return $this;
	}
	public function getItemTitle()
	{
		return (string) $this->_itemTitle;
	}


	/**
	 * @return OrderItem
	 */
	public function setMainCategory($value)
	{
		$this->_mainCategory = (string) $value;
		return $this;
	}
	public function getMainCategory()
	{
		return (string) $this->_mainCategory;
	}

	/**
	 * @return Order
	 */
	public function setReturnShipmentStatus($value)
	{
	    $this->_returnShipmentStatus = (string) $value;
	    return $this;
	}
	public function getReturnShipmentStatus()
	{
	    return (string) $this->_returnShipmentStatus;
	}

}

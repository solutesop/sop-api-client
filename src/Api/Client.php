<?php
namespace SoluteSop\Api;

use SoluteSop\Api\Request\RequestInterface;
use SoluteSop\Api\Request\Orders;
use SoluteSop\Api\Request\Order;
use SoluteSop\Api\Request\OrderLine;

class Client
{

	/**
	 *
	 * @var string
	 */
	protected $_accessToken = null;

	/**
	 *
	 * @var string
	 */
	protected $_version = 'v1';

	/**
	 *
	 * @var string
	 */
	protected $_endpoint = 'https://sop.solute.de/api/';

	/**
	 * simplier method
	 * shorthand
	 *
	 * @param Orders $request
	 * @return \SoluteSop\Api\Response\Orders
	 */
	public function getOrders(Orders $request)
	{
		return $this->call($request);
	}

    /**
     * @param Order $request
     * @return \SoluteSop\Api\Response\Order
     */
    public function getOrder(Order $request)
	{
		return $this->call($request);
	}

    /**
     * @param OrderLine $request
     * @return \SoluteSop\Api\Response\OrderLine
     */
    public function updateOrderLine(OrderLine $request)
	{
		return $this->call($request);
	}


	/**
	 *
	 * @param RequestInterface $request
	 * @return RequestInterface
	 */
	public function call(RequestInterface $request)
	{
		$this->validateSelf();

		$uri = $this->_buildUri($request);

		#var_dump($uri);

		/**
		 * build curl request
		 */

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $uri);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'SoluteSopPHPClient/1.0');
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

		if ($request->isPost()) {
			#$json = file_get_contents($uri);
            /*
			$postParams = $request->buildPostParams();
			$countPostParams = count($postParams);
			$postParamsAsString = $postParams ? $this->_buildParamsAsString($postParams) : '';
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POST, $postParamsAsString);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Accept: application/json'));
            */

            $postParams = $request->buildPostParams();
            $countPostParams = count($postParams);
            $postParams = $postParams ? $this->_buildParamsAsString($postParams) : '';
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POST, $countPostParams);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Accept: application/json'));


			#\Zend_Debug::dump($postParamsAsString);
		} else { // normal get
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
		}

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		$json = curl_exec($ch);
		$errorNumber = curl_errno($ch);
		$error = curl_error($ch);

		$info = curl_getinfo($ch);

		if (!isset($info['http_code'])) {
		    throw new \RuntimeException('Response curl info is not http_code is not set');
		} elseif ($info['http_code'] !== 200) {
		    switch ($info['http_code']) {
		        default:
								\Ubl\Core\Debug::dump($info);
		            throw new \RuntimeException($info['http_code'] . ' API httpCode');
		            break;
		    }
		}

		#var_dump(curl_getinfo($ch));
		#\Ubl\Core\Debug::dump($json);
		#die();

		curl_close($ch);

		if ($error || $errorNumber) {
			throw new \RuntimeException('Call failed (Curl error: (' . $errorNumber . ') ' . $error . ')');
		}

		$response = $request->getResponseObject();
		$response->setRawData($json);

		$hydrator = $response->getHydratorObject();
		$hydrator->hydrate($response);

		return $response;
	}

	protected function _buildUri(RequestInterface $request)
	{
		$uri = $this->getBaseUri() . $request->getMethod();

		// special uris
		$uri = $request->modifyUri($uri);

		$uri .= '?';

		$params = $request->buildGetParams();
		#\Zend_Debug::dump($params);die();
		$params['access_token'] = $this->getAccessToken();

		$uri .= $this->_buildParamsAsString($params);

		return $uri;
	}

	protected function _buildParamsAsString(array $params = null)
	{
		if (!is_array($params)) {
			return '';
		}
		foreach ($params as $key => $value) {
			$stringParams[$key] = $key . '=' . urlencode($value);
		}
		return implode('&', $stringParams);
	}

	public function validateSelf()
	{
		try {
			if (!$this->getEndpoint()) {
				throw new \UnexpectedValueException('Endpoint not set');
			}
			if (!$this->getVersion()) {
				throw new \UnexpectedValueException('Version not set');
			}
			if (!$this->getAccessToken()) {
				throw new \UnexpectedValueException('Access token not set');
			}

			return $this;
		} catch (Exception $e) {
			throw new \RuntimeException('Validation of Client failed (' . $e->getMessage() . ')');
		}
	}


	public function getBaseUri()
	{
		return $this->getEndpoint() . $this->getVersion() . '/';
	}

	/**
	 *
	 * @param string $value
	 * @return Solute_Sop_Model_Api_Client
	 */
	public function setVersion($value)
	{
		$this->_version = $value;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getVersion()
	{
		return $this->_version;
	}

	/**
	 *
	 * @param string $value
	 * @return Solute_Sop_Model_Api_Client
	 */
	public function setAccessToken($value)
	{
		$this->_accessToken = $value;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getAccessToken()
	{
		return $this->_accessToken;
	}

	/**
	 *
	 * @param string $value
	 * @return Solute_Sop_Model_Api_Client
	 */
	public function setEndpoint($value)
	{
		$this->_endpoint = $value;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getEndpoint()
	{
		return $this->_endpoint;
	}





}

<?php

namespace SoluteSop\Api\Request;

class OrderLine extends RequestAbstract
{

	protected $_responseClass = 'SoluteSop\Api\Response\OrderLine';

	/**
	 *
	 * @var string
	 */
	protected $_method = 'orderlines';

	protected $_isPost = true;

	protected $_buildPostParams = array(
		#'line_reference' => 'lineReference',
		'shipping_carrier' => 'shippingCarrier',
		'shipping_carrier_tracking_id' => 'shippingCarrierTrackingId',
		'quantity_confirmed' => 'quantityConfirmed',
		'quantity_shipped' => 'quantityShipped',
	);

	protected $_lineReference = null;

	protected $_shippingCarrier = null;

	protected $_shippingCarrierTrackingId = null;

	protected $_quantityConfirmed = null;

	protected $_quantityShipped = null;


	public function modifyUri($uri)
	{
		return $uri . '/line_reference=' . $this->getLineReference() . '/feedback';
	}

	/**
	 * @return OrderLine
	 */
	public function setLineReference($value)
	{
		$this->_lineReference = (string) $value;
		return $this;
	}
	public function getLineReference()
	{
		return (string) $this->_lineReference;
	}


	/**
	 * @return OrderLine
	 */
	public function setShippingCarrier($value)
	{
		$this->_shippingCarrier = (string) $value;
		return $this;
	}
	public function getShippingCarrier()
	{
		return (string) $this->_shippingCarrier;
	}


	/**
	 * @return OrderLine
	 */
	public function setShippingCarrierTrackingId($value)
	{
		$this->_shippingCarrierTrackingId = (string) $value;
		return $this;
	}
	public function getShippingCarrierTrackingId()
	{
		return (string) $this->_shippingCarrierTrackingId;
	}


	/**
	 * @return OrderLine
	 */
	public function setQuantityConfirmed($value)
	{
		$this->_quantityConfirmed = (int) $value;
		return $this;
	}
	public function getQuantityConfirmed()
	{
		return (int) $this->_quantityConfirmed;
	}


	/**
	 * @return OrderLine
	 */
	public function setQuantityShipped($value)
	{
		$this->_quantityShipped = (int) $value;
		return $this;
	}
	public function getQuantityShipped()
	{
		return (int) $this->_quantityShipped;
	}

}
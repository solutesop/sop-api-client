<?php
namespace SoluteSop\Api\Request;

class Order extends RequestAbstract
{

	protected $_responseClass = 'SoluteSop\Api\Response\Order';

	/**
	 *
	 * @var string
	 */
	protected $_method = 'orders';

	protected $_orderId = null;

	public function modifyUri($uri)
	{
		return $uri . '/' . $this->getOrderId();
	}

	/**
	 *
	 * @param int $value
	 * @return Orders
	 */
	public function setOrderId($value)
	{
		$this->_orderId = (int)$value;
		return $this;
	}

	/**
	 *
	 * @return int
	 */
	public function getOrderId()
	{
		return (int)$this->_orderId;
	}
}
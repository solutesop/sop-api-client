<?php
namespace SoluteSop\Api\Request;

class Orders extends RequestAbstract
{

	const STATE_NONE = 'none';
	const STATE_ANY = 'any';
	const STATE_REDUCED = 'reduced';
	const STATE_COMPLETE = 'complete';

    const STATE_SOME = 'some';
    const STATE_ALL = 'all';



    protected $_responseClass = 'SoluteSop\Api\Response\Orders';

	/**
	 *
	 * @var string
	 */
	protected $_method = 'orders';

	protected $_buildGetParams = array(
		'created_after' => 'dateFrom',
		'created_before' => 'dateTo',
		'limit' => 'limit',
		'offset' => 'offset',
		'confirmation' => 'confirmationState',
		'shipping' => 'shippingState',
        'canceled' => 'canceledState',
	);

	/**
	 *
	 * @var DateTime | null
	 */
	protected $_dateFrom = null;

	/**
	 *
	 * @var DateTime | null
	 */
	protected $_dateTo = null;

	/**
	 *
	 * @var integer
	 */
	protected $_limit = 100;

	/**
	 *
	 * @var integer
	 */
	protected $_offset = 0;

	/**
	 *
	 * @var string
	 */
	protected $_confirmationState = 'any';

	/**
	 * allowed states for confirmation
	 * @var array
	 */
	protected $_confirmationStatesAllowed = array(
        'any', // all orders
        'none', // 0% shipped
        'reduced', // teillieferung
        'complete', // sent to customer
	);

    /**
     * @var string
     */
    protected $_canceledState = 'any';

    protected $_canceledStatesAllowed = array(
        'any',
        'none',
        'some',
        'all',
    );


    /**
	 *
	 * @var string
	 */
	protected $_shippingState = 'any';

	/**
	 * allowed states for shipping
	 * @var array
	 */
	protected $_shippingStatesAllowed = array(
        'any', // all orders
        'none', // 0% shipped
        'reduced', // teillieferung
        'complete', // sent to customer
	);

	/**
	 *
	 * @param string $value
	 * @throws InvalidArgumentException
	 * @return Orders
	 */
	public function setShippingState($value)
	{
		if ($value === null) {
			$this->_shippingState = null;
			return $this;
		}

		$value = (string)$value;
		if (!in_array($value,$this->_shippingStatesAllowed)) {
			throw new \InvalidArgumentException('Value "' . $value . '" for ShippingState is not allowed. Use one of these states (Null, "' . implode('", "', $this->_shippingStatesAllowed). '")');
		}
		$this->_shippingState = $value;
		return $this;
	}

	/**
	 *
	 * @return string|null
	 */
	public function getShippingState()
	{
		return $this->_shippingState;
	}

	/**
	 *
	 * @param string|null $value
	 * @throws InvalidArgumentException
	 * @return Orders
	 */
	public function setConfirmationState($value)
	{
		if ($value === null) {
			$this->_confirmationState = null;
			return $this;
		}

		$value = (string)$value;
		if (!in_array($value,$this->_confirmationStatesAllowed)) {
			throw new \InvalidArgumentException('Value "' . $value . '" for ConfirmationState is not allowed. Use one of these states (Null, "' . implode('", "', $this->_confirmationStatesAllowed). '")');
		}
		$this->_confirmationState = $value;
		return $this;
	}

	/**
	 *
	 * @return string|null
	 */
	public function getConfirmationState()
	{
		return $this->_confirmationState;
	}


    public function setCanceledState($value)
    {
        if ($value === null) {
            $this->_canceledState = null;
            return $this;
        }

        $value = (string)$value;
        if (!in_array($value,$this->_canceledStatesAllowed)) {
            throw new \InvalidArgumentException('Value "' . $value . '" for CanceledState is not allowed. Use one of these states (Null, "' . implode('", "', $this->_canceledStatesAllowed). '")');
        }
        $this->_canceledState = $value;
        return $this;
    }
    public function getCanceledState()
    {
        return $this->_canceledState;
    }


	/**
	 *
	 * @param DateTime $value
	 * @return Orders
	 */
	public function setDateFrom(\DateTime $value)
	{
		$this->_dateFrom = $value;
		return $this;
	}

	/**
	 *
	 * @return \DateTime | null
	 */
	public function getDateFrom()
	{
		return $this->_dateFrom;
	}

	/**
	 *
	 * @param \DateTime $value
	 * @return Orders
	 */
	public function setDateTo(\DateTime $value)
	{
		$this->_dateTo = $value;
		return $this;
	}

	/**
	 *
	 * @return DateTime | null
	 */
	public function getDateTo()
	{
		return $this->_dateTo;
	}


	/**
	 *
	 * @param int $value
	 * @return Orders
	 */
	public function setLimit($value)
	{
		$this->_limit = (int)$value;
		return $this;
	}

	/**
	 *
	 * @return int
	 */
	public function getLimit()
	{
		return (int)$this->_limit;
	}

	/**
	 *
	 * @param int $value
	 * @return Orders
	 */
	public function setOffset($value)
	{
		$this->_offset = (int)$value;
		return $this;
	}

	/**
	 *
	 * @return int
	 */
	public function getOffset()
	{
		return (int)$this->_offset;
	}
	}